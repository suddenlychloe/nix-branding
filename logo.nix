{ colors, mkLogo, mkThumbnail, pkgs ? import <nixpkgs> {} }: with pkgs; with builtins;
let
  format = import ./format.nix { inherit pkgs; };
  orColor = color: format.toCSSString (if isString color then colors.${color} else color);
in {

  full = rec {

    svg = { color, width ? null, height ? null, attrs ? "", name ? "logo.full" }: writeText "${name}.svg"
    (mkLogo {
      inherit attrs;
      ${if width  == null then null else "width"} = width;
      ${if height == null then null else "height"} = height;
      color = orColor color;
    });

    ejs = { color, width ? null, height ? null, attrs ? "", name ? "logo.full" }: writeText "${name}.ejs"
    (mkLogo {
      inherit attrs;
      ${if width  == null then null else "width"} = width;
      ${if height == null then null else "height"} = height;
      color = orColor color;
    });

    png = { color, width ? null, height ? null, name ? "logo.full" }: runCommand "${name}.png" {} ''
      ${inkscape}/bin/inkscape -z --export-filename=$out --export-type=png -w ${toString width} ${svg {
        inherit name color;
        ${if width  == null then null else "width"} = width;
        ${if height == null then null else "height"} = height;
      }}
    '';

   };

  thumbnail = rec {
    svg = { color, background ? "", width ? null, height ? null, attrs ? "", name ? "logo.thumbnail" }:
    writeText "${name}.svg" (mkThumbnail {
      inherit attrs;
      color = if isString color then colors.${color} else format.toCSSString color;
      ${if width  == null then null else "width"} = width;
      ${if height == null then null else "height"} = height;
      background = if background == "" then "" else format.toCSSString background;
    });

    ejs = { color, background ? "", width ? null, height ? null, attrs ? "", name ? "logo.thumbnail" }:
    writeText "${name}.ejs" (mkThumbnail {
      inherit attrs;
      ${if width  == null then null else "width"} = width;
      ${if height == null then null else "height"} = height;
      color = if isString color then color else format.toCSSString color;
      background = if background == "" then "" else format.toCSSString background;
    });

    png = { color, background ? "", width ? null, height ? null, name ? "logo.thumbnail" }: runCommand "${name}.png" {} ''
      ${imagemagick}/bin/convert -background none -density 1200 -resize ${toString width}x${toString height} ${svg {
        inherit color background;
      ${if width  == null then null else "width"} = width;
      ${if height == null then null else "height"} = height;
      }} $out
    '';

  };

}
