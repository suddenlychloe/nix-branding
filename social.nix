{ pkgs ? import <nixpkgs> {}, name, twitterHandle }:
{ title, description, image, permalink, cardType ? "summary_large_image" }:
with pkgs; with lib;
writeText "social.html" ''

<meta content="text/html; charset=UTF-8" name="Content-Type"          />

<meta property="og:type"         content="website"                    />
<meta property="og:title"        content="${title}"                   />
<meta property="og:description"  content="${description}"             />
<meta property="og:image"        content="${image.url}"               />
<meta property="og:image:width"  content="${toString image.width}"    />
<meta property="og:image:height" content="${toString image.height}"   />
<meta property="og:url"          content="${permalink}"               />
<meta property="og:site_name"    content="${name}"                    />

<meta property="twitter:card"        content="${cardType}"                />
<meta property="twitter:description" content="${description}"             />
<meta property="twitter:title"       content="${title}"                   />
<meta property="twitter:url"         content="${permalink}"               />
<meta property="twitter:site"        content="${name}"                    />
<meta property="twitter:creator"     content="${twitterHandle}"           />
<meta property="twitter:image"       content="${image.url}"               />

<title>${title}</title>
<meta name="description" content="${description}" />

''
