Manage your branding with nix
===

This module allows you to provide minimal branding details, and generate files for a variety of purposes.


## Setup

First define a set containing the color palette for your brand:

```nix
# colors.nix
{
   main          = { r = 255; g = 145; b = 120; };
   accent        = { r = 120; g = 200; b = 120; };
   copy-standard = { r =  25; g =  25; b =  25; };
   copy-dark     = { r =   0; g =   0; b =   0; };
}
```


Then you will need to define 2 functions:

```nix
# generate the SVG version of your logo with no background
mkLogo = { color, attrs ? "", width ? 800, height ? 200 }: ''
  <svg
     ${attrs}
     version="1.1"
     viewBox="0 0 57.020126 56.013318"
     width="${toString width}"
     height="${toString height}">
    <path
       d="M 0 Z"
       style="fill:${color}" />
  </svg>
'';

# generate the SVG version of your thumbnail (used for favicons and such)
mkThumbnail = { color, background, attrs ? "", height, width }: ''
  <svg
     ${attrs}
     version="1.1"
     viewBox="0 0 57.020126 56.013318"
     width="${toString width}"
     height="${toString height}">
    ${if background == "" then "" else ''<rect width="100%" height="100%" fill="${background}" />''}
    <path
       d="M 0 Z"
       style="fill:${color}" />
  </svg>
'';
```

These serve as the single source of truth for your brand. All assets from these files.

## Usage

```nix
  brand = import (pkgs.fetchgit {
    url    = https://gitlab.com/fresheyeball/nix-branding.git;
    rev    = "12d425344dba09c1b360b957172b31db93c2d0c4";
    sha256 = "1wm2dmmkcg6i7a34wafkscjs82mm96a4p1wvdhafgfx2zmvwn4d3";
  })
  {
    inherit pkgs mkLogo mkThumbnail colors;
    primary-color    = "main";
    background-color = "accent";
    name             = "MyCompany";
    twitterHandle    = "@mycompany";
  };
```

By passing in these arguments, we recieve a set of branding assets. Including:

- Icons for Android and Chrome
- Icons for Apple touch
- Icon for Safari
- Favicons for all browsers
- Tile for Microsoft
- Html with meta data for all icons
- site.manifest for all icons, and theme colors
- Open graph (og:) tags for social media
- Twitter cards
- Importable representation of your logo in .ejs
- Importable representations for your color palette for
  - sass
  - css
  - elm
  - typescript
  - haskell

## Access

### Logo

```nix
brand.logo.full.svg { color = "main" } # build an svg file of the logo using the "main" color
brand.logo.full.png { color = "accent" } # build a png file of the logo using the "accent" color
brand.logo.thumbnail.png { color = "copy-standard" } # build a png file of the thumbnail using the "copy-standard" color
```

- svg
- ejs
- png

are available in nix as derivations.

### Colors

Some utilities provided

```nix
hexString = brand.format.rgbToHex colors.main;
cssString = brand.format.toCSSString colors.main;
```

Colors can be imported as long as you include the file

```nix
# in your nix build
''
  cp ${brand.colorModules.sass} src/sass/Colors.sass
  cp ${brand.colorModules.typescript} src/typescript/Colors.ts
  cp ${brand.colorModules.haskell} src/haskell/Colors.hs
''
```

Now you can import those files into an appropriate language. Keeping colors
in nix in sync with usage in sass for example.

```sass
@import Colors.sass

body
  color: $copy-standard
  padding: 0

header
  background: $accent
```

```typescript
import { CopyStandard, Accent } from "./Colors"

console.log("check it out, colors defined in nix",
  {CopyStandard, Accent});
```

```haskell
module Main where

import Colors

main = do
  putStrLn "check it out, colors defined in nix"
  putStrLn $ "CopyStandard:" ++ show (getColorHex CopyStandard)
  putStrLn $ "Accent:" ++ show (getColorHex Accent)
```

## Icons

Favicons and icons for mobile are tricky. Luckily a script is provided that takes care of the whole thing.

```nix
''
  # dump all png, svg, and ico files into $out
  # along with site.manifest and related assets
  ${brand.icons.mkIcons brand.logo.thumbnail "$out"}

  # this is the generated html that wires the icons in
  cp ${brand.icons.faviconHTML} src/ejs/favicon.ejs
'';
```

## Social

Obtaining the open graph artifact requires extra config, since this may differ between assets

```nix
  socialHTML = brand.socialHTML {
    title       = "OG Title";
    description = "Description of this UR";
    permalink   = "https://www.example.com";
    image       = {
      url    = "https://www.example.com/og-image.jpg";
      height = 941;
      width  = 1800;
    };
  };
```

Now this snippet can be added to your build

```nix
''
  cp ${socialHTML} src/ejs/social.ejs
''
```
