{ pkgs ? import <nixpkgs> {} }: with pkgs; with lib; with builtins; rec {


toHex' = x: if x == 0 then "0" else let
  intToHex = [
    "0" "1" "2" "3" "4" "5" "6" "7" "8" "9"
    "A" "B" "C" "D" "E" "F"
  ];
  toHex'' = q: a:
    if q > 0 then (toHex'' (q / 16) ((elemAt intToHex (lib.mod q 16)) + a)) else a;
  in toHex'' x "";


toHex = x:
  let bothN = splitDecimal "${toString (x / 16.0)}";
      splitDecimal = splitString ".";
      f = y: toHex' (toInt (substring 0 2 y));
      g = y: toHex' (toInt (head (splitDecimal (toString (fromJSON "0.${y}" * 16)))));
  in "${f (lib.head bothN)}${g (lib.last bothN)}";


rgbToHex = {r,g,b}: let b' = if b == 0 then "00" else toHex' b; in
  "#${toHex r}${toHex g}${b'}";


toCSSString = {r,g,b}: "rgb(${toString r},${toString g},${toString b})";


capitalize = x:
  let asChars = stringToCharacters x;
  in concatStrings ([(lib.toUpper (head asChars))] ++ tail asChars);


toPascalCase = x:
  concatStrings (map capitalize (lib.splitString "-" x));

}
